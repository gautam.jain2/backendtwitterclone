const express = require('express')
const cors = require('cors')
const dbSetup = require('./db-setup')

const PORT = process.env.PORT || 3200


const auth = require('./routes/auth')
const userData = require('./routes/userData')
const app = express()
dbSetup()

app.use(express.json())
app.use(cors())
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS');
    res.setHeader(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    next();
  });

// Routes----
app.get('/', (req, res) => {
    res.send('working')
})
app.use('/auth', auth)
app.use('/userData', userData)

app.listen(PORT, () => {
    console.log('Server is running on port 3200')
})
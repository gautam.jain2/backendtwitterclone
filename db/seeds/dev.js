
exports.seed = async function (knex) {

  await knex.raw('TRUNCATE TABLE "users" CASCADE');
  await knex('users').insert([
    {
      "id": 3,
      "name": "gj",
      "email": "gj@gmail.com",
      "handle": "gj",
      "password": "gj",
      "image": "https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png",
      "created_at": "2022-01-14T11:17:40.476Z",
      "updated_at": "2022-01-14T11:17:40.476Z"
    },
    {
      "id": 4,
      "name": "Elon musk",
      "email": "elon@gmail.com",
      "handle": "elon musk",
      "password": "$2b$08$DvNwwCl1mYRdzvxLXULqS.AjrgnFuYHR844F5jFb6cm/6iSzCEjTu",
      "image": "https://pbs.twimg.com/profile_images/856983737426423809/6jebtwP-_400x400.jpg",
      "created_at": "2022-01-14T15:55:42.400Z",
      "updated_at": "2022-01-14T15:55:42.400Z"
    }
  ])

  await knex('tweets').insert([
    {
      "id": 1,
      "tweet": "My first tweet",
      "userId": 3,
      "user": {
        "id": 3,
        "name": "gj",
        "email": "gj@gmail.com",
        "handle": "gj",
        "password": "gj",
        "image": "https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png",
        "created_at": "2022-01-14T11:17:40.476Z",
        "updated_at": "2022-01-14T11:17:40.476Z"
      }
    }
  ])

  await knex('followings').insert([
    {
      "id": 4,
      "userId": 3,
      "followingId": 4
    },
    
  ])

  

};

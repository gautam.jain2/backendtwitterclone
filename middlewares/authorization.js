module.exports = async (req, res, next) => {
    try {
        req.user = req.query.id
        next()

    } catch (error) {
        console.log(error.message)
        return res.status(403).send({ message: 'Unauthorized' })
    }
}
const Users = require('../db/models/users')
module.exports.addUser=async(body)=>{
    let { name, email, handle, image, password } = body;
    const user = await Users.query().where('email', email)
        if (user.length !== 0) {
            return res.status(401).send("User already exists")
        }
        const insertedUser = await Users.query().insert({
            name: name,
            email: email,
            handle: handle,
            image: image,
            password: password
        })
        return insertedUser
}
module.exports.checkLogin=async(body)=>{
    let { email} = body
    const user = await Users.query().where('email', email)
    return user
}
const Users = require('../db/models/users')
const Tweets = require('../db/models/tweets')
const Followings = require('../db/models/followings')

module.exports.user=async()=>{
    const users = await Users.query()
    return users;
}
module.exports.allTweets=async()=>{
    const tweets = await Tweets.query().withGraphFetched('user')
    return tweets
}
module.exports.allFollowings=async()=>{
    const followings = await Followings.query()
    return followings
}
module.exports.homePageTweets=async(id)=>{
    const followings = await Followings.query()
    const peopleFollowedByUser = followings.filter((item) => item.userId == id)
    let userIds = [];
    for (let i = 0; i < peopleFollowedByUser.length; i++) {
        userIds.push(peopleFollowedByUser[i]['followingId'])
    }
    console.log(userIds)
    
        const tweet = await Tweets.query().withGraphFetched('user')
        const result = tweet.filter(o1 => userIds.some(o2 => o1.userId?o2:id));
    return result.reverse()
}
module.exports.me=async(email)=>{
    const profile = await Users.query().where('email', email)
    return profile
}
module.exports.whoToFollow=async(id)=>{
    const allUsers = await Users.query()
        const followingUsers = await Followings.query().where('userId', id)
        const followingIds = {}
        followingIds[id] = true
        for (let i = 0; i < followingUsers.length; i++) {
            followingIds[followingUsers[i]['followingId']] = true
        }
        const whoToFollow = []
        for (let i = 0; i < allUsers.length; i++) {
            let userId = allUsers[i]['id'].toString()
            if (followingIds[userId] == true) {
                continue
            } else {
                whoToFollow.push(allUsers[i])
            }
        }
        return whoToFollow

}
module.exports.userFollowings=async (id)=>{
    const followings = await Followings.query()
    const peopleFollowedByUser = followings.filter((item) => item.userId == id)
    let userIds = [];
    for (let i = 0; i < peopleFollowedByUser.length; i++) {
        userIds.push(peopleFollowedByUser[i]['followingId'])
    }
    let users = [];
    for (let i = 0; i < userIds.length; i++) {
        const user = await Users.query().where('id', userIds[i])
        users.push(user[0])
    }
    return users
}
module.exports.userFollowers=async (id)=>{
    const followings = await Followings.query()
    const peopleFollowedByUser = followings.filter((item) => item.followingId == id)
    let userIds = [];
    for (let i = 0; i < peopleFollowedByUser.length; i++) {
        userIds.push(peopleFollowedByUser[i]['userId'])
    }
    console.log(peopleFollowedByUser)
    let users = [];
    for (let i = 0; i < userIds.length; i++) {
        const user = await Users.query().where('id', userIds[i])
        users.push(user[0])
    }
    return users
}
module.exports.follow=async(id,followingId)=>{
    const insertedFollowing = await Followings.query().insert({
        userId: id,
        followingId: followingId
    })
    return insertedFollowing

}
module.exports.unfollow=async(id,followingId)=>{
    const removeFollowing = await Followings.query().where('userId', id).where('followingId', followingId)
    const toBeDeleted = removeFollowing[0]['id']
    const followingRemoved = await Followings.query().delete().where('id', toBeDeleted)
    return followingRemoved
}
module.exports.myTweets=async(id)=>{
    const myTweets = await Tweets.query().where('userId', id).withGraphFetched('user')
    return myTweets.reverse()
}
module.exports.addTweet=async (id,tweet)=>{
    const myTweets = await Tweets.query().insert({
        tweet: tweet,
        userId: id
    })
    return myTweets

}
const router = require('express').Router();
const userData=require("../controllers/userData.controllers")
const authorization = require('../middlewares/authorization')

router.get('/users',userData.users)

router.get('/tweets',userData.tweets)

router.get('/followings', userData.followings)

router.get('/homepageTweets',authorization, userData.homepageTweets)

router.get('/me', userData.me)
router.get('/whoToFollow',authorization, userData.whoToFollow)
router.get('/userFollowings',authorization, userData.userFollowings)
router.get('/userFollowers',authorization, userData.userFollowers)

router.post('/follow',authorization, userData.follow)

router.post('/unFollow',authorization,userData.unfollow)

router.get('/mytweets', authorization,userData.myTweets)

router.post('/tweet',authorization,userData.tweet)

router.get('/homePageData', userData.homepagedata)

module.exports = router
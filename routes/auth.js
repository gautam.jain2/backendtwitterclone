const router = require('express').Router();

const auth=require("../controllers/auth.controllers")


router.post('/register',auth.register)

router.post('/login',auth.login)

router.get('/verify', auth.verify)


module.exports = router
const userData=require("../services/userData.services")
module.exports.users=async (req, res,next) => {
    try{
   const users=await userData.user()
    res.status(200).send(users)
    next()
    }
    catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.tweets=async (req, res,next) => {
    const tweets=await userData.allTweets()
    res.status(200).send(tweets)
    next()
}
module.exports.followings=async (req, res,next) => {
    const followings=await userData.allFollowings()
    res.send(followings)
    next()
}
module.exports.homepageTweets=async (req, res,next) => {
    try {
       const tweets=await userData.homePageTweets(req.user)
        res.status(200).send(tweets)
        next()
    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.me=async (req, res,next) => {
    try {
        const profile=await userData.me(req.query.email)
        res.status(200).send(profile)
        next()


    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.whoToFollow=async (req, res,next) => {
    try {
        const id=req.user
        const whoToFollow=await userData.whoToFollow(id)
        res.status(200).send(whoToFollow)
        next()

    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.userFollowings= async (req, res,next) => {
    try {
        const id = req.user
       const users=await userData.userFollowings(id)
        res.send(users)
        next()


    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.userFollowers= async (req, res,next) => {
    try {
        const id = req.user
       const users=await userData.userFollowers(id)
        res.send(users)
        next()
    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.follow=async (req, res, next) => {

    try {
        const id = req.user
        const { followingId } = req.body
       const insertedFollowing=await userData.follow(id,followingId)
        res.status(200).send(insertedFollowing)
        next()

    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }

}
module.exports.unfollow= async (req, res, next) => {
    try {
        const id = req.user
        const { followingId } = req.body
        const removeFollowing=await userData.unfollow(id,followingId)
        res.status(200).send(removeFollowing)
        next()

    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.myTweets=async (req, res, next) => {
    try {
        const id=req.user
        const myTweets=await userData.myTweets(id)
        res.status(200).send(myTweets)
        next()

    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.tweet= async (req, res,next) => {
    try {
        const id = req.user
        const { tweet } = req.body
        const myTweets=await userData.addTweet(id,tweet)
        res.send(myTweets)
        next()

    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.homepagedata=async (req, res) => {
    const id = req.user

}
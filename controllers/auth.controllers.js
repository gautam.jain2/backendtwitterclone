const auth=require("../services/auth.services")
module.exports.register= async (req, res, next) => {
    try {
        const res=await auth.addUser(req.body)
        console.log(res)
        res.status(200).send('success')
        next()
    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.login=async (req, res, next) => {
    try {
        const user=await auth.checkLogin(req.body)
        res.status(200).send(user)
        next()
    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
}
module.exports.verify=async (req, res) => {
    try {
        res.send({ verified: true })

    } catch (error) {
        console.log(error.message)
        res.status(500).send({ verified: false })
    }
}